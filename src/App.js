import React from 'react';
import { Admin, Resource } from 'react-admin';
// import jsonServerProvider from 'ra-data-json-server';
import { UserList, UserCreate, UserShow, UserEdit } from './admin_components/users.js';
import { RoleList } from './admin_components/roles.js';
// import { PostList, PostEdit, PostCreate } from './admin_components/posts.js';
// import PostIcon from '@material-ui/icons/Book';
import UserIcon from '@material-ui/icons/Group';
import Dashboard from './admin_components/Dashboard.js';
import authProvider from './admin_components/authProvider.js';
import { createMuiTheme } from '@material-ui/core/styles';
import dataProvider from './admin_components/dataProvider.js';
import Menu from './testComponents/Menu.js';
import MyLayout from './testComponents/MyLayout.js';

// const dataProvider = jsonServerProvider('http://jsonplaceholder.typicode.com');

// const theme = createMuiTheme({
//   palette: {
//     type: 'dark', // Switching the dark mode on is a single property value change.
//   },
// });

const App = () => {
  return (
    <Admin dashboard={Dashboard} dataProvider={dataProvider} authProvider={authProvider}>
      	<Resource name="users" list={UserList} show={UserShow} create={UserCreate} edit={UserEdit} delete={true} icon={UserIcon} />
      	<Resource name="roles" list={RoleList} />
    </Admin>
  );
}

export default App;
