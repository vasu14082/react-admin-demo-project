import React from 'react';
import { List, Datagrid, TextField, EmailField, ReferenceField, DateField, Create, SimpleForm, TextInput, required, Show, SimpleShowLayout, Edit, DateInput, EditButton, Filter, Pagination, SaveButton } from 'react-admin';
// import MyUrlField from './MyUrlField';
// import ChevronLeft from '@material-ui/icons/ChevronLeft';
// import ChevronRight from '@material-ui/icons/ChevronRight';
// import ResetViewsButton from './ResetViewsButton.js';

// const users = (props) => {


// }

// export default users;

// export const users = props => (
// 	// {console.log("users", props);}
//     <List {...props}>
//         <Datagrid rowClick="edit">
//             <TextField source="id" />
//             <TextField source="name" />
//             <TextField source="username" />
//             <EmailField source="email" />
//             <TextField source="address.street" />
//             <TextField source="phone" />
//             <MyUrlField source="website" />
//             <TextField source="company.name" />
//         </Datagrid>
//     </List>
// );

const UserFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Search" source="q" alwaysOn />
        <TextInput label="email" source="email" allowEmpty />
    </Filter>
);

export const UserList = props => {
    // console.log("UserList", props);
    return (
    <List filters={<UserFilter />} pagination={<Pagination />} {...props} bulkActionButtons={false}>
        <Datagrid rowClick="show">
            <TextField source="id" />
            <TextField source="user_id" />
            <EmailField source="email" />
            <TextField source="username" />
            <TextField source="first_name" />
            <TextField source="last_name" />
            <TextField source="status" />
            <DateField source="created_at" />
            <DateField source="updated_at" />
            <EditButton />
        </Datagrid>
    </List>
)};

export const UserCreate = props => (
    <Create {...props} redirect="show">
        <SimpleForm>
            <TextInput source="username" validate={[required()]} />
            <TextInput source="email" validate={[required()]} />
            <TextInput source="first_name" validate={[required()]} />
            <TextInput source="last_name" validate={[required()]} />
            <TextInput source="userroles" validate={[required()]} />
            <TextInput source="dialer_id" />
            <TextInput source="status" validate={[required()]} />
            <TextInput source="password" validate={[required()]} />
            <TextInput source="name" />

        </SimpleForm>
    </Create>
);

export const UserShow = props => (
    <Show {...props}>
        <SimpleShowLayout>
            <TextField source="id" />
            <TextField source="user_id" />
            <EmailField source="email" />
            <DateField source="username" />
            <TextField source="first_name" />
            <TextField source="last_name" />
            <DateField source="userroles" />
            <ReferenceField source="dialer_id" reference="dialers"><TextField source="id" /></ReferenceField>
            <TextField source="status" />
            <TextField source="permissions" />
            <DateField source="created_at" />
            <DateField source="updated_at" />
        </SimpleShowLayout>
    </Show>
);

export const UserEdit = props => (
    <Edit {...props}>
        <SimpleForm toolbar={<SaveButton />}>
            <TextInput source="id" />
            <TextInput source="user_id" />
            <TextInput source="email" />
            <TextInput source="username" />
            <TextInput source="first_name" />
            <TextInput source="last_name" />
            <TextInput source="userroles" />
            <TextInput source="status" />
            <TextInput source="permissions" />
            <DateInput source="created_at" />
            <DateInput source="updated_at" />
        </SimpleForm>
    </Edit>
);