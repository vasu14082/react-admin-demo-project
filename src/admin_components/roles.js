import React from 'react';
import { List, Datagrid, TextField, ArrayField, SingleFieldList, ChipField, DateField } from 'react-admin';

export const RoleList = props => (
    <List {...props}>
        <Datagrid rowClick="edit">
            <TextField source="id" />
            <TextField source="display_name" />
            <TextField source="name" />
            <DateField source="created_at" />
            <DateField source="updated_at" />
            <ArrayField source="permissions"><SingleFieldList><ChipField source="id" /></SingleFieldList></ArrayField>
        </Datagrid>
    </List>
);