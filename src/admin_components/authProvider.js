export default {
    // called when the user attempts to log in
    login: ({ username, password }) =>  {
        const request = new Request('https://lmscrm-new.a2zportals.co.in/api/auth/login', {
            method: 'POST',
            body: JSON.stringify({ username:username, password:password }),
            headers: new Headers({ 'Content-Type': 'application/json' }),
        });
        return fetch(request)
            .then(response => {
                if (response.status < 200 || response.status >= 300) {
                    throw new Error(response.statusText);
                }
                // console.log("login",response);
                return response.json();
            })
            .then(({access_token}) => {
                // console.log("then",props);
                localStorage.setItem('token', access_token);
                // Promise.resolve();
            });
    },
    // called when the user clicks on the logout button
    logout: () => {
        localStorage.removeItem('token');
        return Promise.resolve();
    },
    // called when the API returns an error
     checkError: (error) => {
        const status = error.status;
        if (status === 401 || status === 403) {
            console.log("checkError",error.status);
            // localStorage.removeItem('token');
            return Promise.reject();
        }
        return Promise.resolve();
    },
    // called when the user navigates to a new location, to check for authentication
    checkAuth: () => {
        return localStorage.getItem('token')
            ? Promise.resolve()
            : Promise.reject();
    },
    // called when the user navigates to a new location, to check for permissions / roles
    getPermissions: () => Promise.resolve(),
};